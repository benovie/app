angular.module('App', [
	'ngMaterial', 
	'ngRoute',
	'pascalprecht.translate',
	'App.Account'
]).config(['$routeProvider','$translateProvider', function(routeProvider, translateProvider) {
	routeProvider.when('/login', {
		templateUrl : 'modules/account/templates/login.html'
	});
	routeProvider.otherwise({
		templateUrl : 'resources/templates/main.html'
	});
}]).run(['$rootScope', '$location', '$route', '$mdSidenav', function($rootScope, $location, $route, $mdSidenav) {
	$rootScope.$on('$routeChangeError', function(event, currentRoute, previousRoute, rejection) {
		 if (rejection === 'NotAuthenticated') {
			 $location.path('/login');
		 }
	});
	$rootScope.toggleSidenav = function(sidenavId) {
		$mdSidenav(sidenavId).toggle();
	};
	$rootScope.$route = $route;
}]);