module.exports = function(grunt) {
	grunt.initConfig({
		concat: {
			css: {
 				src: [
 					'resources/css/import.css',
 					'bower_components/angular/angular-csp.css',
 					'bower_components/angular-material/angular-material.min.css',
 					'resources/css/app.css',
 					'modules/**/*.css'
 				],
 				dest: 'build/app.css'
			},
			libJs: {
 				src: [
 					'bower_components/angular/angular.min.js',
 					'bower_components/angular-aria/angular-aria.min.js',
 					'bower_components/angular-animate/angular-animate.min.js',
 					'bower_components/angular-material/angular-material.min.js',
 					'bower_components/angular-sanitize/angular-sanitize.min.js',
 					'bower_components/angular-translate/angular-translate.min.js',
 					'bower_components/angular-route/angular-route.min.js'
 				],
 				dest: 'build/lib.js'
			},
			allJs: {
 				src: [
 					'build/lib.js',
 					'build/app.js',
 					'build/templates.js'
 				],
 				dest: 'build/all.js'
			}			
 		},
		uglify: {
			appJs: {
 				src: [
 					'resources/js/*.js',
 					'modules/**/*.js'
 				],
 				dest: 'build/app.js'
			}			
 		},
        ngtemplates:   {
		  app:    {
			    src:      ['modules/**/*.html'],
			    dest:     'build/templates.js',
				options : {
					module : 'App',
					htmlmin: {
					  collapseBooleanAttributes:      true,
					  collapseWhitespace:             true,
					  removeAttributeQuotes:          true,
					  removeComments:                 true,
					  removeEmptyAttributes:          true,
					  removeRedundantAttributes:      true,
					  removeScriptTypeAttributes:     true,
					  removeStyleLinkTypeAttributes:  true
					}	
				}
		  }
		},
 		watch: {
		    moduleHtml: {
		      files: ['modules/**/*.html'],
		      tasks: ['ngtemplates'],
		    },
            css: {
		      files: ['resources/css/*.css', 'modules/**/*.css'],
		      tasks: ['concat:css'],
		    },
		    appJs: {
		      files: ['resources/js/*.js', 'modules/**/*.js'],
		      tasks: ['uglify:appJs'],
		    },
		    js: {
		      files: ['build/app.js', 'build/templates.js', 'build/lib.js'],
		      tasks: ['concat:allJs'],
		    }
  		}
	});
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-angular-templates');
	grunt.registerTask('default', ['ngtemplates','uglify', 'concat']);
}